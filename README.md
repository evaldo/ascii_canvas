# Ascii Canvas

## Instructions to run
The phoenix project was created using `mix phx.new ascii_canvas --live`.

The flow to run it is the same as any phoenix project. 
You'll need erlang, elixir, node/npm and postgres to run it.

* `mix deps.get`
* `mix ecto.setup`
* `npm install --prefix assets`
* `mix phx.server`

It will run the project on port 4000 and you can acess the web
client on in your browser visiting [`localhost:4000`](http://localhost:4000).
More details on the API and Web client below.

---------
## Tests

The greatest number of the core tests were develed with using TDD, while the other parts of the system were developed as new functionalities were added.

There are some key tests tagged with `fixtures` that were directly taken from the requirements.

To run tests use `mix test`.

----------
## How was it done?

The core of the system lives in the module `AsciiCanvas.Paint.Canvas`, this is where the structure of the canvas is created and the operations are done. 

The data structure started simple and kept like this until the end, as lists of lists, the first dimension representing the lines and the second the columns.

The canvas can start empty or with a initial size, where all "pixels" are set as `nil`.

Rectangules can be drawned in any `x` and `y` coordinates with any `width` and `height`, negative values are accepted. The canvas will grow to contain the rectangule.

As the requirements stated, one of fill or outline values must be given to draw the rectangule. If only the filling value is given, it will be used as the outline as well.

The flooding operation is done recursively until a character different from the one at the starting coordinates or canvas boundaries is met.


-----------
## Rest API
The api uses basic phoenix controllers/views. Errors are handled using a fallback controller that renders a json object with the errors. 

HTTP response statuses are respected

The endpoints:

#### POST at `/api/v1/paintings`: creates a new painting

Request body:

* `width`: integer/optional. Determines the initial width of the painting.

* `height`:  integer/optional. Determines the initial height of the painting.

Both ignored if any is zero.

Response body inside `data` key:

* `id`: id of the created painting

* `canvas`: current canvas. First dimension of the list is the lines and second the columns.

Response headers:

* `location` the path resource of the created painting


#### POST at `/api/v1/paintings/:id/rectangules`: draw a rectangule in the painting

Request body:

* `x` integer/obrigatory. The column of the start of the rectangule.

* `y` integer/obrigatory. The row of the start of the rectangule.

* `width` integer/obrigatory. The width of the rectangule.

* `height` integer/obrigatory. The height of the rectangule.

* `fill` string length 1. The filling char of the rectangule.

* `outline` string length 1. The outline char of the rectangule. If not provieded, `fill` will be used as outline.

One of `fill` or `outline` must be provided. Integers can assume negative values.

Response body inside `data` key:

* `id`: id of the created painting

* `canvas`: current canvas. First dimension of the list is the lines and second the columns.

#### POST at `/api/v1/paintings/:id/floods`: floods a coordinate in the painting

Request body:

* `x` integer/obrigatory. The column of the start of the flood.

* `y` integer/obrigatory. The row of the start of the flood.

* `fill` string length 1/obrigatory. The filling char of the flood.

Response body inside `data` key:

* `id`: id of the created painting

* `canvas`: current canvas. First dimension of the list is the lines and second the columns.


#### GET at `/api/v1/paintings/:id`: show a painting

Response body inside `data` key:

* `id`: id of the created painting

* `canvas`: current canvas. First dimension of the list is the lines and second the columns.

-----------------
## Web Client

Served in the root path `/`.

Developed using phoenix live view.

Changes done in the paintings, be it from the api or from another web client user, are published in the pubsub and any connected web client is updated.

New records also updates the page.

The basic drawing operations are supported by the web client, to do it you must fill the form above the canvas and then click in a table cell to draw the rectangule/flood, being that cell the `x` and `y` of the operation.

I'm not expert at creating UI/UX , so do not expect a lot in this regard haha

But the basic user experience is there (I think :sweat_smile:)
