defmodule AsciiCanvas.Repo.Migrations.RenamePaintingRectangulesToShapes do
  use Ecto.Migration

  def change do
    rename table(:paintings), :rectangules, to: :shapes
  end
end
