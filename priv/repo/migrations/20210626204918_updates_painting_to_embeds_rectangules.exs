defmodule AsciiCanvas.Repo.Migrations.UpdatesPaintingToEmbedsRectangules do
  use Ecto.Migration

  def change do
    alter table(:paintings) do
      remove :canvas
      add :rectangules, {:array, :jsonb}
    end
  end
end
