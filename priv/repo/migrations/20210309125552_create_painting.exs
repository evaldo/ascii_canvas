defmodule AsciiCanvas.Repo.Migrations.CreatePainting do
  use Ecto.Migration

  def change do
    create table(:paintings) do
      add :canvas, {:array, {:array, :string}}

      timestamps()
    end
  end
end
