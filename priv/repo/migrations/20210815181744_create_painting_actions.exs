defmodule AsciiCanvas.Repo.Migrations.CreatePaintingActions do
  use Ecto.Migration

  def change do
    create table(:painting_actions) do
      add :action, :string
      add :payload, {:array, :map}
      add :painting_id, references(:paintings)

      timestamps()
    end
  end
end
