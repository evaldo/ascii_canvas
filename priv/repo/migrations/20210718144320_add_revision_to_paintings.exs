defmodule AsciiCanvas.Repo.Migrations.AddRevisionToPaintings do
  use Ecto.Migration

  def change do
    alter table(:paintings) do
      add :revision, :integer
    end
  end
end
