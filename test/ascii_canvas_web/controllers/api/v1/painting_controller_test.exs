defmodule AsciiCanvasWeb.Api.V1.PaintingControllerTest do
  use AsciiCanvasWeb.ConnCase

  import Phoenix.ConnTest

  describe "create" do
    test "when params are valid", %{conn: conn} do
      conn = post(conn, Routes.painting_path(conn, :create), %{})
      assert %{"data" => %{"id" => _, "canvas" => []}} = json_response(conn, 201)
    end
  end

  describe "show" do
    test "when painting is empty", %{conn: conn} do
      {:ok, %{id: id}} = AsciiCanvas.Paint.create_painting()

      conn = get(conn, Routes.painting_path(conn, :show, id))

      assert %{"data" => %{"canvas" => []}} = json_response(conn, 200)
    end

    test "when painting has rectangules", %{conn: conn} do
      {:ok, %{id: id} = painting} = AsciiCanvas.Paint.create_painting()

      {:ok, _} =
        painting
        |> AsciiCanvas.Paint.draw_rectangule(%{x: -10, y: 2, width: 4, height: 5, fill: "A", revision: painting.revision})

      conn = get(conn, Routes.painting_path(conn, :show, id))

      assert %{
               "data" => %{
                 "id" => _,
                 "canvas" => [
                   [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
                   [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
                   ["A", "A", "A", "A", nil, nil, nil, nil, nil, nil],
                   ["A", "A", "A", "A", nil, nil, nil, nil, nil, nil],
                   ["A", "A", "A", "A", nil, nil, nil, nil, nil, nil],
                   ["A", "A", "A", "A", nil, nil, nil, nil, nil, nil],
                   ["A", "A", "A", "A", nil, nil, nil, nil, nil, nil]
                 ]
               }
             } = json_response(conn, 200)
    end

    test "when painting does not exist", %{conn: conn} do
      assert_error_sent :not_found, fn ->
        get(conn, Routes.painting_path(conn, :show, -100))
      end
    end
  end

  describe "draw_rectangules" do
    test "when params are valid", %{conn: conn} do
      {:ok, %{id: id, revision: revision}} = AsciiCanvas.Paint.create_painting()

      conn =
        post(conn, Routes.painting_path(conn, :draw_rectangules, id), %{
          x: 2,
          y: 2,
          width: 4,
          height: 4,
          fill: "A",
          outline: "-",
          revision: revision
        })

      assert %{
               "data" => %{
                 "canvas" => [
                   [nil, nil, nil, nil, nil, nil],
                   [nil, nil, nil, nil, nil, nil],
                   [nil, nil, "-", "-", "-", "-"],
                   [nil, nil, "-", "A", "A", "-"],
                   [nil, nil, "-", "A", "A", "-"],
                   [nil, nil, "-", "-", "-", "-"]
                 ]
               }
             } = json_response(conn, 200)
    end

    test "when fill and outline are not provided", %{conn: conn} do
      {:ok, %{id: id}} = AsciiCanvas.Paint.create_painting()

      conn =
        post(conn, Routes.painting_path(conn, :draw_rectangules, id), %{
          x: 2,
          y: 2,
          width: 4,
          height: 4,
          fill: "",
          outline: ""
        })

      assert %{
               "fill" => ["fill or outline must be provided"],
               "outline" => ["fill or outline must be provided"]
             } == json_response(conn, 422)
    end

    test "when fill and outline are invalid", %{conn: conn} do
      {:ok, %{id: id}} = AsciiCanvas.Paint.create_painting()

      conn =
        post(conn, Routes.painting_path(conn, :draw_rectangules, id), %{
          x: 2,
          y: 2,
          width: 4,
          height: 4,
          fill: "AA",
          outline: "BB"
        })

      assert %{
               "fill" => ["should be at most 1 character(s)"],
               "outline" => ["should be at most 1 character(s)"]
             } == json_response(conn, 422)
    end

    test "when painting does not exist", %{conn: conn} do
      assert_error_sent :not_found, fn ->
        get(conn, Routes.painting_path(conn, :draw_rectangules, -1), %{
          x: 2,
          y: 2,
          width: 4,
          height: 4,
          fill: "A",
          outline: "B"
        })
      end
    end
  end

  describe "flood" do
    test "when params are valid", %{conn: conn} do
      {:ok, painting} = AsciiCanvas.Paint.create_painting()

      {:ok, painting} =
        AsciiCanvas.Paint.draw_rectangule(painting, %{
          x: 0,
          y: 0,
          width: 10,
          height: 10,
          fill: "B",
          revision: painting.revision
        })

      conn =
        post(conn, Routes.painting_path(conn, :flood, painting.id), %{
          x: 2,
          y: 2,
          fill: "A",
          revision: painting.revision
        })

      assert %{
               "data" => %{
                 "canvas" => [
                   ["A", "A", "A", "A", "A", "A", "A", "A", "A", "A"],
                   ["A", "A", "A", "A", "A", "A", "A", "A", "A", "A"],
                   ["A", "A", "A", "A", "A", "A", "A", "A", "A", "A"],
                   ["A", "A", "A", "A", "A", "A", "A", "A", "A", "A"],
                   ["A", "A", "A", "A", "A", "A", "A", "A", "A", "A"],
                   ["A", "A", "A", "A", "A", "A", "A", "A", "A", "A"],
                   ["A", "A", "A", "A", "A", "A", "A", "A", "A", "A"],
                   ["A", "A", "A", "A", "A", "A", "A", "A", "A", "A"],
                   ["A", "A", "A", "A", "A", "A", "A", "A", "A", "A"],
                   ["A", "A", "A", "A", "A", "A", "A", "A", "A", "A"]
                 ]
               }
             } = json_response(conn, 200)
    end

    test "when fill and outline are not provided", %{conn: conn} do
      {:ok, %{id: id}} = AsciiCanvas.Paint.create_painting()

      conn =
        post(conn, Routes.painting_path(conn, :flood, id), %{
          x: 2,
          y: 2
        })

      assert %{
               "fill" => ["can't be blank"]
             } == json_response(conn, 422)
    end
  end
end
