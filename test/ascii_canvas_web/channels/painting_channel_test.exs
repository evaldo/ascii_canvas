defmodule AsciiCanvasWeb.PaintingChannelTest do
  use AsciiCanvasWeb.ChannelCase

  setup do
    {:ok, painting} = AsciiCanvas.Paint.create_painting()

    [painting: painting]
  end

  test "draw rectangule", %{painting: %{id: id, revision: revision}} do
    {:ok, socket} = connect(AsciiCanvasWeb.UserSocket, %{})
    {:ok, _, socket} = subscribe_and_join(socket, "painting:#{id}")

    ref = push(socket, "draw_rect", %{x: 0, y: 0, width: 10, height: 10, fill: "1", revision: revision})

    assert_reply ref, :ok, %{
      painting: %{
        id: ^id,
        revision: 1,
        shapes: [
          _bounds,
          %{
            fields: %{
              fill: "1",
              outline: nil,
              x: 0,
              y: 0,
              width: 10,
              height: 10,
            },
            type: "Rectangule"
          }
        ]
      }
    }
  end

  test "drawing rectangule with invalid params", %{painting: %{id: id}} do
    {:ok, socket} = connect(AsciiCanvasWeb.UserSocket, %{})
    {:ok, _, socket} = subscribe_and_join(socket, "painting:#{id}")

    ref = push(socket, "draw_rect", %{x: 0, y: 0, width: 10, height: 10, fill: "1 "})

    assert_reply ref, :error, %{
      painting: %{
        id: ^id,
        revision: 0,
        shapes: [
          _bounds,
        ]
      },
      errors: %{
        fill: ["should be at most 1 character(s)"]
      }
    }
  end

  test "draw flood" do
    {:ok, %{id: id, revision: revision}} = AsciiCanvas.Paint.create_painting(%{width: 10, height: 10})

    {:ok, socket} = connect(AsciiCanvasWeb.UserSocket, %{})
    {:ok, _, socket} = subscribe_and_join(socket, "painting:#{id}")

    ref = push(socket, "draw_flood", %{x: 0, y: 0, fill: "1", revision: revision})

    assert_reply ref, :ok, %{
      painting: %{
        id: ^id,
        revision: 1,
        shapes: [
          %{fields: %{height: 10, width: 10}, type: "Bounds"},
          %{fields: %{fill: "1", x: 0, y: 0}, type: "Flood"}
        ]
      }
    }
  end

  test "drawing flood with invalid params", %{painting: %{id: id}} do
    {:ok, socket} = connect(AsciiCanvasWeb.UserSocket, %{})
    {:ok, _, socket} = subscribe_and_join(socket, "painting:#{id}")

    ref = push(socket, "draw_flood", %{x: 0, y: 0, fill: "1 "})

    assert_reply ref, :error, %{
      painting: %{
        id: ^id,
        revision: 0,
        shapes: [
          _bounds,
        ]
      },
      errors: %{
        fill: ["should be at most 1 character(s)"]
      }
    }
  end

  test "when drawing without giving current version", %{painting: %{id: id}} do
    {:ok, socket} = connect(AsciiCanvasWeb.UserSocket, %{})
    {:ok, _, socket} = subscribe_and_join(socket, "painting:#{id}")

    ref = push(socket, "draw_flood", %{x: 0, y: 0, fill: "1"})

    assert_reply ref, :error, %{
      errors: %{
        revision: ["not given or different from the one in the server"]
      }
    }
  end


end
