defmodule AsciiCanvasWeb.PaintingsChannelTest do
  use AsciiCanvasWeb.ChannelCase

  setup do
    {:ok, painting} = AsciiCanvas.Paint.create_painting()

    [painting: painting]
  end

  test "list current paintings", %{painting: %{id: id}} do
    {:ok, socket} = connect(AsciiCanvasWeb.UserSocket, %{})

    {:ok, _, socket} = subscribe_and_join(socket, "paintings")

    ref = push(socket, "list")

    assert_reply ref, :ok, [
      %{
        id: ^id,
        revision: 0,
        shapes: [_bounds]
      }
    ]
  end

  test "create new canvas without bounds" do
    {:ok, socket} = connect(AsciiCanvasWeb.UserSocket, %{})
    {:ok, _, socket} = subscribe_and_join(socket, "paintings")

    ref = push(socket, "create", %{})

    assert_reply ref, :ok, %{
      painting: %{shapes: [_bounds]}
    }
  end

  test "create new canvas with bounds" do
    {:ok, socket} = connect(AsciiCanvasWeb.UserSocket, %{})
    {:ok, _, socket} = subscribe_and_join(socket, "paintings")

    ref = push(socket, "create", %{width: 10, height: 10})

    assert_reply ref, :ok, %{
      painting: %{shapes: [
        %{fields: %{width: 10, height: 10}, type: "Bounds"}
      ]}
    }
  end

  test "create new canvas with invalid bounds" do
    {:ok, socket} = connect(AsciiCanvasWeb.UserSocket, %{})
    {:ok, _, socket} = subscribe_and_join(socket, "paintings")

    ref = push(socket, "create", %{width: -10, height: 10})

    assert_reply ref, :error, %{
      error: %{width: ["must not be negative"]}
    }
  end
end
