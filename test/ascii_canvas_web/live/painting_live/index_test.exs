defmodule AsciiCanvasWeb.PaintingLive.IndexTest do
  use AsciiCanvasWeb.ConnCase, async: false

  import Phoenix.LiveViewTest

  alias AsciiCanvas.Paint

  describe "index" do
    test "when there are no paintings", %{conn: conn} do
      assert {:ok, _view, html} = live(conn, "/")

      assert html =~ "No painting created yet :("
    end

    test "when there are paintings", %{conn: conn} do
      {:ok, painting} = Paint.create_painting()

      {:ok, view, _html} = live(conn, "/")

      assert to_string(painting.id) ==
               view
               |> render()
               |> Floki.parse_fragment!()
               |> Floki.find("#paintings")
               |> Floki.text()
    end

    test "when a painting is created in other client", %{conn: conn} do
      {:ok, view, html} = live(conn, "/")

      assert html =~ "No painting created yet :("

      {:ok, painting} = Paint.create_painting()

      assert to_string(painting.id) ==
               view
               |> render()
               |> Floki.parse_fragment!()
               |> Floki.find("#paintings")
               |> Floki.text()
    end
  end

  describe "new" do
    test "when creating new painting", %{conn: conn} do
      {:ok, view, _html} = live(conn, Routes.painting_index_path(conn, :new))

      view
      |> form("#painting-form")
      |> render_submit()

      render(view)

      [%{id: last_id}] = Paint.list_last_paintings(1)

      assert_patch(view, "/#{last_id}")
    end
  end

  describe "show" do
    test "when showing a rectangule 2x2", %{conn: conn} do
      {:ok, painting} = Paint.create_painting()
      {:ok, painting} = Paint.draw_rectangule(painting, %{x: 0, y: 0, width: 2, height: 2, fill: "X", revision: painting.revision})

      {:ok, view, html} = live(conn, Routes.painting_index_path(conn, :show, painting))

      assert manager = Map.get(:sys.get_state(:paint_registry), painting.id)

      assert view.pid in :sys.get_state(manager).clients

      assert html =~ "Showing painting #{painting.id}"

      assert ["X", "X", "X", "X"] == get_canvas(view)
    end

    test "when showing a rectangule 2x2 in a canvas 3x3", %{conn: conn} do
      {:ok, painting} = Paint.create_painting(%{width: 3, height: 3})

      {:ok, painting} = Paint.draw_rectangule(painting, %{x: 0, y: 0, width: 2, height: 2, fill: "X", revision: painting.revision})

      {:ok, view, html} = live(conn, Routes.painting_index_path(conn, :show, painting))

      assert html =~ "Showing painting #{painting.id}"

      assert ["X", "X", "", "X", "X", "", "", "", ""] == get_canvas(view)
    end

    test "when rectangule is draw with invalid params", %{conn: conn} do
      {:ok, painting} = Paint.create_painting()

      {:ok, view, _html} = live(conn, Routes.painting_index_path(conn, :show, painting))

      html =
        view
        |> form("#rectangule-form")
        |> render_change(%{})

      assert [
               "",
               "can't be blank",
               "",
               "can't be blank",
               "",
               "fill or outline must be provided",
               "",
               "fill or outline must be provided",
               ""
             ] ==
               Floki.parse_document!(html)
               |> Floki.raw_html(pretty: true)
               |> Floki.find("#rectangule-form .invalid-feedback")
               |> Floki.text()
               |> String.split("\n")
               |> Enum.map(&String.trim/1)
    end

    test "when rectangule is draw with valid params", %{conn: conn} do
      {:ok, painting} = Paint.create_painting()

      {:ok, painting} =
        Paint.draw_rectangule(painting, %{x: 0, y: 0, width: 3, height: 3, fill: " ", revision: painting.revision})

      {:ok, view, _html} = live(conn, Routes.painting_index_path(conn, :show, painting))

      view
      |> element("#rectangule-form")
      |> render_change(%{
        rectangule: %{width: 2, height: 2, outline: "x"}
      })

      view
      |> element("[data-xy=1-1]")
      |> render_click()

      assert ["", "", "", "", "x", "x", "", "x", "x"] == get_canvas(view)
    end

    test "when flood is changed with invalid form", %{conn: conn} do
      {:ok, painting} = Paint.create_painting()

      {:ok, view, _html} = live(conn, Routes.painting_index_path(conn, :show, painting))

      html =
        view
        |> form("#flood-form")
        |> render_change(%{})

      assert [
               "",
               "can't be blank",
               ""
             ] ==
               Floki.parse_document!(html)
               |> Floki.raw_html(pretty: true)
               |> Floki.find("#flood-form .invalid-feedback")
               |> Floki.text()
               |> String.split("\n")
               |> Enum.map(&String.trim/1)
    end

    test "when flood is clicked with valid form", %{conn: conn} do
      {:ok, painting} = Paint.create_painting()

      {:ok, painting} =
        Paint.draw_rectangule(painting, %{x: 0, y: 0, width: 3, height: 3, fill: "T", revision: painting.revision})

      {:ok, view, _html} = live(conn, Routes.painting_index_path(conn, :show, painting))

      view
      |> element("#operation-flood")
      |> render_click()

      view
      |> form("#flood-form")
      |> render_change(%{
        flood: %{fill: "a"}
      })

      view
      |> element("[data-xy=1-1]")
      |> render_click()

      assert ["a", "a", "a", "a", "a", "a", "a", "a", "a"] == get_canvas(view)
    end

    test "when painting is updated in other client", %{conn: conn} do
      {:ok, painting} = Paint.create_painting()

      {:ok, view, _html} = live(conn, Routes.painting_index_path(conn, :show, painting))

      assert [] == get_canvas(view)

      {:ok, _} =
        painting
        |> Paint.draw_rectangule(%{x: 0, y: 0, width: 1, height: 1, fill: "1", revision: painting.revision})

      assert ["1"] == get_canvas(view)
    end
  end

  defp get_canvas(view) do
    view
    |> render()
    |> Floki.parse_fragment!()
    |> Floki.find("#canvas td")
    |> Enum.map(&Floki.text/1)
    |> Enum.map(&String.trim/1)
  end
end
