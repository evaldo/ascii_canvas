defmodule AsciiCanvas.PaintTest do
  use AsciiCanvas.DataCase, async: true

  alias AsciiCanvas.Paint
  alias AsciiCanvas.Paint.Painting
  alias AsciiCanvas.Paint.Canvas

  describe "create/1" do
    test "when creating a new empty painting" do
      assert {:ok, %Painting{shapes: [%{width: 0, height: 0}]}} = Paint.create_painting()
    end

    test "when creating a new empty painting with bounds" do
      {:ok, painting} = Paint.create_painting(%{width: 10, height: 10})

      assert [%{height: 10, width: 10}] = painting.shapes
      assert [%{}] = painting.actions
    end
  end

  describe "draw_rectangule/2" do
    test "when drawing rectangule on empty painting" do
      {:ok, painting} = Paint.create_painting()

      assert {:ok, %{shapes: [_bounds, rect]} = painting} =
               Paint.draw_rectangule(painting, %{x: 2, y: 2, width: 2, height: 2, fill: "a", revision: painting.revision})

      assert %{x: 2, y: 2, width: 2, height: 2, fill: "a", outline: nil} = rect

      assert [
               [nil, nil, nil, nil],
               [nil, nil, nil, nil],
               [nil, nil, "a", "a"],
               [nil, nil, "a", "a"]
             ] == Canvas.render(painting.shapes)
    end
  end

  describe "flood/3" do
    test "when flooding on a painting" do
      {:ok, painting} = Paint.create_painting()

      assert 0 == painting.revision

      {:ok, painting} =
        Paint.draw_rectangule(painting, %{x: 0, y: 0, width: 4, height: 4, fill: "a", revision: painting.revision})

      assert 1 == painting.revision

      assert {:ok, painting} = Paint.flood(painting, %{x: 0, y: 0, fill: "b", revision: painting.revision})

      assert 2 == painting.revision

      assert [
               ["b", "b", "b", "b"],
               ["b", "b", "b", "b"],
               ["b", "b", "b", "b"],
               ["b", "b", "b", "b"]
             ] == Canvas.render(painting.shapes)
    end
  end

  describe "undo/1" do
    test "when undoing an empty canvas" do
      {:ok, painting} = Paint.create_painting()

      assert [%{}] = painting.shapes
      assert [%{}] = painting.actions

      {:ok, painting} = Paint.undo(painting, %{painting: painting.revision})

      assert [%{}] = painting.shapes
      assert [%{}] = painting.actions
    end


    test "when undoing a rectangule" do
      {:ok, painting} = Paint.create_painting()
      {:ok, painting} = Paint.draw_rectangule(painting, %{x: 0, y: 0, width: 10, height: 10, fill: "A", revision: painting.revision})

      assert [%{}, %{}] = painting.shapes
      assert [%{}, %{}] = painting.actions

      {:ok, painting} = Paint.undo(painting, %{revision: painting.revision})

      assert [%{}] = painting.shapes
      assert [%{}, %{}, %{action: "undo"}] = painting.actions
    end
  end
end
