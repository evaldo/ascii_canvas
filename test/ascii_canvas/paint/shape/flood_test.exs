defmodule AsciiCanvas.Paint.Shape.FloodTest do
  use AsciiCanvas.DataCase, async: true

  alias AsciiCanvas.Paint.Shape.Flood

  test "when no params are given" do
    assert changeset = Flood.changeset(%Flood{}, %{})

    assert %{
             fill: ["can't be blank"],
             x: ["can't be blank"],
             y: ["can't be blank"]
           } == errors_on(changeset)
  end

  test "when params are valid" do
    assert %{valid?: true} =
             Flood.changeset(%Flood{}, %{
               x: 0,
               y: 0,
               fill: "A"
             })
  end

  test "when fill has more than one char" do
    assert changeset =
             Flood.changeset(%Flood{}, %{
               x: 0,
               y: 0,
               fill: "AA"
             })

    assert ["should be at most 1 character(s)"] == errors_on(changeset).fill
  end

  test "when fill is empty char" do
    assert %{valid?: true} =
             Flood.changeset(%Flood{}, %{
               x: 0,
               y: 0,
               fill: " "
             })
  end
end
