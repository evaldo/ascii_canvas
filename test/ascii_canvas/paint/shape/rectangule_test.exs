defmodule AsciiCanvas.Paint.Shape.RectanguleTest do
  use AsciiCanvas.DataCase, async: true

  alias AsciiCanvas.Paint.Shape.Rectangule

  test "when no params are given" do
    assert changeset = Rectangule.changeset(%Rectangule{}, %{})

    errors = errors_on(changeset)

    assert %{
             fill: ["fill or outline must be provided"],
             height: ["can't be blank"],
             outline: ["fill or outline must be provided"],
             width: ["can't be blank"],
             x: ["can't be blank"],
             y: ["can't be blank"]
           } == errors
  end

  test "when params are valid with fill" do
    assert %{valid?: true} =
             Rectangule.changeset(%Rectangule{}, %{
               x: 0,
               y: 0,
               width: 10,
               height: 10,
               fill: "A"
             })
  end

  test "when params are valid with outline" do
    assert %{valid?: true} =
             Rectangule.changeset(%Rectangule{}, %{
               x: 0,
               y: 0,
               width: 10,
               height: 10,
               outline: "A"
             })
  end

  test "when fill has more than one char" do
    changeset =
      Rectangule.changeset(%Rectangule{}, %{
        x: 0,
        y: 0,
        width: 10,
        height: 10,
        fill: "AA"
      })

    assert ["should be at most 1 character(s)"] == errors_on(changeset).fill
  end

  test "when outline has more than one char" do
    changeset =
      Rectangule.changeset(%Rectangule{}, %{
        x: 0,
        y: 0,
        width: 10,
        height: 10,
        outline: "AA"
      })

    assert ["should be at most 1 character(s)"] == errors_on(changeset).outline
  end
end
