defmodule AsciiCanvas.Paint.CanvasTest do
  use AsciiCanvas.DataCase, async: true

  alias AsciiCanvas.Paint.Canvas
  alias AsciiCanvas.Paint.Shape.{
    Rectangule,
    Flood,
    Bounds
  }

  doctest Canvas

  test "when rectangule 1x1 is drawned at 0x0" do
    assert [
             ["."]
           ] ==
             Canvas.render([
               %Rectangule{x: 0, y: 0, width: 1, height: 1, fill: "."}
             ])
  end

  test "when rectangule 2x1 is drawned at 0x0" do
    assert [
             [".", "."]
           ] ==
             Canvas.render([
               %Rectangule{x: 0, y: 0, width: 2, height: 1, fill: "."}
             ])
  end

  test "when rectangule 1x2 is drawned at 0x0" do
    assert [
             ["."],
             ["."]
           ] ==
             Canvas.render([
               %Rectangule{x: 0, y: 0, width: 1, height: 2, fill: "."}
             ])
  end

  test "when rectangule 2x2 is drawned at 0x0" do
    assert [
             [".", "."],
             [".", "."]
           ] ==
             Canvas.render([
               %Rectangule{x: 0, y: 0, width: 2, height: 2, fill: "."}
             ])
  end

  test "when rectangule 1x1 is drawned at 1x0" do
    assert [
             [nil, "."]
           ] ==
             Canvas.render([
               %Rectangule{x: 1, y: 0, width: 1, height: 1, fill: "."}
             ])
  end

  test "when rectangule 1x1 is drawned at 1x1" do
    assert [
             [nil, nil],
             [nil, "."]
           ] ==
             Canvas.render([
               %Rectangule{x: 1, y: 1, width: 1, height: 1, fill: "."}
             ])
  end

  test "when rectangule 3x1 is drawned at 5x0" do
    assert """
                ...\
           """ ==
             Canvas.render([
               %Rectangule{x: 5, y: 0, width: 3, height: 1, fill: "."}
             ])
             |> Canvas.to_string()
  end

  test "when rectangule 7x6 is drawned at 14x0" do
    assert """
                         .......
                         .......
                         .......
                         .......
                         .......
                         .......\
           """ ==
             Canvas.render([
               %Rectangule{x: 14, y: 0, width: 7, height: 6, fill: "."}
             ])
             |> Canvas.to_string()
  end

  test "when rectangule 1x1 is drawned on existing canvas" do
    assert "XY" ==
             Canvas.render([
               %Rectangule{x: 0, y: 0, width: 1, height: 1, fill: "X"},
               %Rectangule{x: 1, y: 0, width: 1, height: 1, fill: "Y"}
             ])
             |> Canvas.to_string()
  end

  test "when drawing only filled fixture 1" do
    assert """
                                   \n\
                                   \n\
              @@@@@                \n\
              @@@@@  XXXXXXXXXXXXXX
              @@@@@  XXXXXXXXXXXXXX
                     XXXXXXXXXXXXXX
                     XXXXXXXXXXXXXX
                     XXXXXXXXXXXXXX
                     XXXXXXXXXXXXXX\
           """ ==
             Canvas.render([
               %Rectangule{x: 3, y: 2, width: 5, height: 3, fill: "@"},
               %Rectangule{x: 10, y: 3, width: 14, height: 6, fill: "X"}
             ])
             |> Canvas.to_string()
  end

  test "when outlined rectangule 1x1 is drawned at 0x0" do
    assert "@" ==
             [
               %Rectangule{x: 0, y: 0, width: 1, height: 1, outline: "@"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when outlined rectangule 3x3 is drawned at 0x0" do
    assert """
           @@@
           @X@
           @@@\
           """ ==
             [
               %Rectangule{x: 0, y: 0, width: 3, height: 3, fill: "X", outline: "@"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when drawing outlined fixture 1" do
    assert """
                                   \n\
                                   \n\
              @@@@@                \n\
              @XXX@  XXXXXXXXXXXXXX
              @@@@@  XOOOOOOOOOOOOX
                     XOOOOOOOOOOOOX
                     XOOOOOOOOOOOOX
                     XOOOOOOOOOOOOX
                     XXXXXXXXXXXXXX\
           """ ==
             [
               %Rectangule{x: 3, y: 2, width: 5, height: 3, fill: "X", outline: "@"},
               %Rectangule{x: 10, y: 3, width: 14, height: 6, fill: "O", outline: "X"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when drawing fixture 2" do
    assert """
                         .......
                         .......
                         .......
           OOOOOOOO      .......
           O      O      .......
           O    XXXXX    .......
           OOOOOXXXXX           \n\
                XXXXX           \
           """ ==
             [
               %Rectangule{x: 14, y: 0, width: 7, height: 6, fill: "."},
               %Rectangule{x: 0, y: 3, width: 8, height: 4, outline: "O"},
               %Rectangule{x: 5, y: 5, width: 5, height: 3, fill: "X", outline: "X"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when flooding an empty canvas" do
    assert "" ==
             [
               %Flood{x: 0, y: 0, fill: "0"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when flooding a rectangule 1x1 at 0x0" do
    assert "I" ==
             [
               %Rectangule{x: 0, y: 0, width: 1, height: 1, fill: "Y"},
               %Flood{x: 0, y: 0, fill: "I"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when flooding a rectangule 2x2 at 0x0" do
    assert """
           II
           II\
           """ ==
             [
               %Rectangule{x: 0, y: 0, width: 2, height: 2, fill: "X"},
               %Flood{x: 0, y: 0, fill: "I"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when flooding a rectangule 3x3 at 0x0 with no outline" do
    assert """
           III
           III
           III\
           """ ==
             [
               %Rectangule{x: 0, y: 0, width: 3, height: 3, fill: "X"},
               %Flood{x: 0, y: 0, fill: "I"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when flooding a rectangule 3x3 at 0x0 with outline" do
    assert """
           III
           IXI
           III\
           """ ==
             [
               %Rectangule{x: 0, y: 0, width: 3, height: 3, fill: "X", outline: "O"},
               %Flood{x: 0, y: 0, fill: "I"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when flooding a rectangule 3x3 at 1x1 with outline" do
    assert """
           OOO
           OIO
           OOO\
           """ ==
             [
               %Rectangule{x: 0, y: 0, width: 3, height: 3, fill: "X", outline: "O"},
               %Flood{x: 1, y: 1, fill: "I"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when flooding the outline of rectangule 4x4 at 0x0" do
    assert """
           IIII
           IXXI
           IXXI
           IIII\
           """ ==
             [
               %Rectangule{x: 0, y: 0, width: 4, height: 4, fill: "X", outline: "O"},
               %Flood{x: 0, y: 0, fill: "I"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when flooding the insides of rectangule 4x4 at 1x1" do
    assert """
           OOOO
           OIIO
           OIIO
           OOOO\
           """ ==
             [
               %Rectangule{x: 0, y: 0, width: 4, height: 4, fill: "X", outline: "O"},
               %Flood{x: 1, y: 1, fill: "I"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when flooding the emptness of the canvas" do
    assert """
           IIIIIIIIIIIIII
           IIIIIIIIIIIIII
           IIIIIIIIIIIIII
           IIIIIIIIIIIIII
           IIIIIIIIIIIIII
           IIIIIIIIIIIIII
           IIIIIIIIIIIIII
           IIIIIIIIIIIIII
           IIIIIIIIIIIIII
           IIIIIIIIIIIIII
           IIIIIIIIIIXXXX
           IIIIIIIIIIXXXX
           IIIIIIIIIIXXXX
           IIIIIIIIIIXXXX\
           """ ==
             [
               %Rectangule{x: 10, y: 10, width: 4, height: 4, fill: "X"},
               %Flood{x: 1, y: 1, fill: "I"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when flooding 2 merged rectangules" do
    assert """
                       \n\
                       \n\
                       \n\
              .....    \n\
              .....    \n\
              .........
              .........
              .....    \
           """ ==
             [
               %Rectangule{x: 3, y: 3, width: 5, height: 5, fill: "X"},
               %Rectangule{x: 5, y: 5, width: 7, height: 2, fill: "X"},
               %Flood{x: 5, y: 5, fill: "."}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when drawing fixture 3" do
    assert """
           --------------.......
           --------------.......
           --------------.......
           OOOOOOOO------.......
           O      O------.......
           O    XXXXX----.......
           OOOOOXXXXX-----------
                XXXXX-----------\
           """ ==
             [
               %Rectangule{x: 14, y: 0, width: 7, height: 6, fill: "."},
               %Rectangule{x: 0, y: 3, width: 8, height: 4, outline: "O"},
               %Rectangule{x: 5, y: 5, width: 5, height: 3, fill: "X", outline: "X"},
               %Flood{x: 0, y: 0, fill: "-"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when rect has negative coordinates" do
    assert """
           aaaaaaaaaa
           aaaaaaaaaa
           aaaaaaaaaa
           aaaaaaaaaa
           aaaaaaaaaa
           aaaaaaaaaa
           aaaaaaaaaa
           aaaaaaaaaa
           aaaaaaaaaa
           aaaaaaaaaa\
           """ ==
             [
               %Rectangule{x: -10, y: -10, width: 10, height: 10, fill: "a"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when rect has negative coordinates on existing canvas" do
    assert """
           bb           \n\
           bb           \n\
                        \n\
                        \n\
                        \n\
                        \n\
                        \n\
                        \n\
                        \n\
                        \n\
                        \n\
                        \n\
                       a
                       a
                       a
                       a
                       a\
           """ ==
             [
               %Rectangule{x: 2, y: 2, width: 1, height: 5, fill: "a"},
               %Rectangule{x: -10, y: -10, width: 2, height: 2, fill: "b"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when rect has negative width and height" do
    assert """
           aaaaa
           aaaaa\
           """ ==
             [
               %Rectangule{x: 0, y: 0, width: -5, height: -2, fill: "a"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when two rects are drawn at 0x0, one positive height and the other negative" do
    assert """
           aa
           aa
           bb\
           """ ==
             [
               %Rectangule{x: 0, y: 0, width: 2, height: 2, fill: "b"},
               %Rectangule{x: 0, y: 0, width: 2, height: -2, fill: "a"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when drawing multiple negative width and height rects" do
    assert """
           c      \n\
                  \n\
                  \n\
              bb  \n\
              bb  \n\
                 a\
           """ ==
             [
               %Rectangule{x: 2, y: 2, width: -1, height: -1, fill: "a"},
               %Rectangule{x: 0, y: 0, width: -2, height: 2, fill: "b"},
               %Rectangule{x: -3, y: -3, width: -1, height: -1, fill: "c"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when flooding outbounds coordinate" do
    assert "A" ==
             [
               %Rectangule{x: 0, y: 0, width: 1, height: 1, fill: "A"},
               %Flood{x: 10, y: 10, fill: "B"}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when flooding the canvas with the same fill again should not overflow" do
    assert """
                     \n\
                     \n\
                     \n\
                     \n\
                     \n\
                     \n\
                     \n\
                     \n\
                     \n\
                     \
           """ ==
             [
               %Rectangule{x: 0, y: 0, width: 10, height: 10, fill: "A"},
               %Flood{x: 5, y: 5, fill: " "},
               %Flood{x: 5, y: 5, fill: " "}
             ]
             |> Canvas.render()
             |> Canvas.to_string()
  end

  test "when canvas has width 1" do
    assert [] ==
      [
        %Bounds{width: 1, height: 0}
      ]
      |> Canvas.render()
  end

  test "when canvas has height 1" do
    assert [] ==
      [
        %Bounds{width: 0, height: 1}
      ]
      |> Canvas.render()
  end

  test "when canvas has height 1 width 1" do
    assert [[nil]] ==
      [
        %Bounds{width: 1, height: 1}
      ]
      |> Canvas.render()
  end

  test "when canvas has height 2 width 1" do
    shapes =
      [
        %Bounds{width: 1, height: 2}
      ]

    assert [[nil], [nil]] == Canvas.render(shapes)
    assert """
            \n\
            \
           """ ==
      shapes
      |> Canvas.render()
      |> Canvas.to_string()
  end

  test "when canvas has height 1 width 2" do
    shapes =
      [
        %Bounds{width: 2, height: 1}
      ]

    assert [[nil, nil]] == Canvas.render(shapes)
    assert """
             \
           """ ==
      shapes
      |> Canvas.render()
      |> Canvas.to_string()
  end

  test "when canvas has height 2 width 2" do
    shapes =
      [
        %Bounds{width: 2, height: 2}
      ]

    assert [[nil, nil], [nil, nil]] == Canvas.render(shapes)
    assert """
             \n\
             \
           """ ==
      shapes
      |> Canvas.render()
      |> Canvas.to_string()
  end
end
