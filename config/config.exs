# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ascii_canvas,
  ecto_repos: [AsciiCanvas.Repo]

# Configures the endpoint
config :ascii_canvas, AsciiCanvasWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "gHeI43GZQhFacOX/6+0FsjkUhqEY+wIfLB5Yl1BH91VdOTPxhbzGcRivTkmip7A8",
  render_errors: [view: AsciiCanvasWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: AsciiCanvas.PubSub,
  live_view: [signing_salt: "m3xfnvy1"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
