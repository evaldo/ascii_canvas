import React from 'react'
import { PageNew } from './PageNew'
import { PageShow } from './PageShow'

export const Body = ({
  currentPage,
  onNewPainting,
  setOperation,
  currentPainting,
  currentRendered,
  onDrawRect,
  onDrawFlood,
  onUndo,
  operation,
  channel
}) => {
  switch (currentPage) {
    case "index":
      return (
        <>
          <h3>Select a painting on the list to start editing or click in the "New Painting" link to start a new one</h3>
        </>
      )

    case "new":
      return <PageNew onNewPainting={onNewPainting} />

    case "show":
      return <PageShow
        currentPainting={currentPainting}
        setOperation={setOperation}
        currentRendered={currentRendered}
        channel={channel}
        onDrawRect={onDrawRect}
        onDrawFlood={onDrawFlood}
        operation={operation}
        onUndo={onUndo}
      />
  }
}