export const normalize = (rect) => {
  const [x, width] =
    rect.width < 0
      ? [rect.x - Math.abs(rect.width) + 1, Math.abs(rect.width)]
      : [rect.x, rect.width]

  const [y, height] =
    rect.height < 0
      ? [rect.y - Math.abs(rect.height) + 1, Math.abs(rect.height)]
      : [rect.y, rect.height]

  return { ...rect, x, y, width, height }

}

const rectRightBottom = ({ x, y, width, height }) => {
  return [x + width - 1, y + height - 1]
}

export const inside = ([x, y], rect) => {
  const [rightX, rightY] = rectRightBottom(rect)

  return x >= rect.x && x <= rightX && y >= rect.y && y <= rightY
}

const dimensions = (canvas) => {
  return {
    x: canvas.length > 0 ? canvas[0].length : 0,
    y: canvas.length
  }
}

const inbounds = (canvas, x, y) => {
  if (x >= 0 && y >= 0) {
    const { x: maxX, y: maxY } = dimensions(canvas)

    return x < maxX && y < maxY
  }

  return false
}

const outline = ([x, y], rect) => {
  const [rightX, rightY] = rectRightBottom(rect)

  return x == rect.x || x == rightX || y == rect.y || y == rightY
}

const renderRect = (canvas, rect) => {
  rect = normalize(rect)

  const canvasInitialX = Math.min(0, rect.x)
  const canvasInitialY = Math.min(0, rect.y)

  const { x: currentWidth, y: currentHeight } = dimensions(canvas)

  const canvasFinalX = Math.max(currentWidth, rect.x + rect.width)
  const canvasFinalY = Math.max(currentHeight, rect.y + rect.height)

  const result = []
  for (let y = canvasInitialY; y < canvasFinalY; y++) {
    result.push([])
    for (let x = canvasInitialX; x < canvasFinalX; x++) {
      if (inside([x, y], rect)) {
        if (outline([x, y], rect)) {
          result[y].push(rect.outline || rect.fill)
        } else {
          result[y].push(rect.fill)
        }
      } else {
        result[y].push((canvas[y] || [])[x])
      }
    }
  }

  return result
}

const floodPixel = ({ canvas, visited }, x, y, fill, initial) => {
  const shouldChange = (canvas[y] || [])[x] == initial && !visited.includes(`${x},${y}`)

  if (shouldChange && inbounds(canvas, x, y)) {
    canvas = canvas.map(x => [...x])
    canvas[y][x] = fill

    visited = [...visited, `${x},${y}`]

    let res = floodPixel({ canvas, visited }, x - 1, y, fill, initial)
    res = floodPixel(res, x + 1, y, fill, initial)
    res = floodPixel(res, x, y - 1, fill, initial)
    return floodPixel(res, x, y + 1, fill, initial)
  } else {
    return { canvas, visited: [...visited, `${x},${y}`] }
  }
}

const renderFlood = (canvas, flood) => {
  const initial = canvas[flood.y][flood.x]

  let { canvas: flooded } = floodPixel({ canvas: canvas, visited: [] }, flood.x, flood.y, flood.fill, initial)

  return flooded
}

const renderBounds = (_canvas, bounds) => {
  const canvas = []
  for (let y = 0; y < bounds.height; y++) {
    canvas.push([])

    for (let x = 0; x < bounds.width; x++) {
      canvas[y].push(null)
    }
  }

  return canvas
}

export const render = (canvas, shape) => {
  if (shape.type == 'Rectangule')
    return renderRect(canvas, shape.fields)

  if (shape.type == 'Flood')
    return renderFlood(canvas, shape.fields)

  if (shape.type == 'Bounds')
    return renderBounds(canvas, shape.fields)

  throw new Error("What shape is it? " + shape.type)
}

export const renderPainting = (painting) => {
  return painting.shapes.reduce((acc, rect) => render(acc, rect), [])
}