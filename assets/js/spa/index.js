import React, { useEffect, useState } from 'react'
import ReactDOM from 'react-dom'
import { Body } from './Body'
import { offlineRepo, OnlineRepo } from './repo'
import { Sync } from './Sync'

const App = () => {
  const [desiredState, setDesiredState] = useState(true)
  const [isOnline, setOnline] = useState(false)
  const [repo, setRepo] = useState()
  const [paintings, setPaintings] = useState([])
  const [currentPainting, setCurrentPainting] = useState()
  const [currentPage, setCurrentPage] = useState("index")
  const [operation, setOperation] = useState("rectangule")

  const defaultErrorHandler = (errors) => {
    alert(JSON.stringify(errors, null, 4))
  }

  useEffect(() => {
    if (desiredState) {
      const onlineRepo =
        new OnlineRepo()

      const timeout = setTimeout(() => {
        if (!isOnline) {
          onlineRepo.disconnect()
          setDesiredState(false)
        }
      }, 5000)

      onlineRepo.initialize({
        onError: () => {
          setOnline(false)
        },
        onClose: () => {
          setOnline(false)
        },
        onConnected: () => {
          clearTimeout(timeout)
          setRepo(onlineRepo)
          setOnline(true)
          setCurrentPage('sync')
        }
      })

    } else {
      repo && repo.disconnect && repo.disconnect()
      offlineRepo.initialize({
        onUpdateListing: setPaintings
      })
      setRepo(offlineRepo)
      setOnline(false)
    }
  }, [desiredState])

  const onNewPainting = (params) => {
    repo.create(params)
      .then((painting) => loadPainting(painting))
      .catch(defaultErrorHandler)
  }

  const loadPainting = (painting) => {
    repo.managePainting(painting, {
      onPaintingUpdated: (painting) => {
        setCurrentPainting(painting)
      }
    })
      .then(() => setCurrentPage("show"))
      .catch(defaultErrorHandler)
  }

  const onDrawAction = (shapeAction) => (rect) => {
    repo.draw(shapeAction, currentPainting, rect)
      .then((painting) => setCurrentPainting(painting))
      .catch(defaultErrorHandler)
  }

  const onDrawRect = onDrawAction("draw_rect")
  const onDrawFlood = onDrawAction("draw_flood")
  const onUndo = onDrawAction("undo")

  const toggleConnectivity = () => setDesiredState(!desiredState)

  const onSyncEnd = () => {
    setCurrentPage("index")
    repo.manageListing({
      onUpdateListing: setPaintings
    })
  }

  if (currentPage === 'sync')
    return <Sync offlineRepo={offlineRepo} onlineRepo={repo} onSyncEnd={onSyncEnd} />

  return (
    <div>
      <section className="row">
        <div className="column">
          Status: {
            isOnline
              ? "Online"
              : (desiredState ? "Connecting..." : "Offline")
          }
        </div>
        <button onClick={toggleConnectivity}>
          {
            isOnline
              ? (desiredState ? "Go offline" : "Disconnect")
              : "Connect"
          }
        </button>
      </section>

      <section className="row">
        <div className="column column-20">
          <a href="#" onClick={() => setCurrentPage("new")}>New Painting</a>
          <br />
          <br />
          <h4>Last paintings</h4>
          {paintings.length === 0 && "No painting created yet :("}
          <ul id="paintings">
            {paintings.map((painting) => (
              <li key={painting.id}>
                <a href="#" onClick={() => loadPainting(painting)} data-testid={`show-painting-${painting.id}`}>
                  {painting.id}
                </a>
              </li>
            ))}
          </ul>
        </div>
        <div className="column">
          <Body
            currentPage={currentPage}
            onNewPainting={onNewPainting}
            setOperation={setOperation}
            currentPainting={currentPainting}
            operation={operation}
            onDrawRect={onDrawRect}
            onDrawFlood={onDrawFlood}
            onUndo={onUndo}
          />
        </div>
      </section>
    </div>
  )
}

if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/service-worker.js').then((registration) =>
      console.info("registered", registration)
    )
  })
}

ReactDOM.render(<App />, document.querySelector("#react-app"))

