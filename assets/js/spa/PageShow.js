import React, { useState, useEffect, useMemo } from 'react'
import { inside, normalize, render, renderPainting } from "./renderer"

export const RenderedCanvas = ({ preview, cellAttrs, ratio = 1 }) => {
  const cellSize = 20
  const canvasBounds =
    useMemo(() => ({
      width: preview.length > 0 ? preview[0].length * cellSize : 0,
      height: preview.length * cellSize
    })
      , [preview])

  return (
    <div style={{ width: (canvasBounds.width * ratio) + 2, height: (canvasBounds.height * ratio) + 2, border: "solid 1px" }}>
      <svg width={canvasBounds.width} height={canvasBounds.height} viewBox={`0 0 ${canvasBounds.width / ratio} ${canvasBounds.width / ratio}`}>
        {preview.map((y, iy) => {
          return y.map((x, ix) => {
            return (
              <g key={`${ix}-${iy}`}>
                <text
                  style={{ userSelect: "none" }}
                  x={ix * cellSize}
                  y={(iy * cellSize) + cellSize}
                >
                  {x || " "}
                </text>
                <rect
                  width={cellSize}
                  height={cellSize}
                  x={ix * cellSize}
                  y={iy * cellSize}
                  {...(cellAttrs ? cellAttrs(ix, iy) : {})}
                />
              </g>
            )
          })
        })}
      </svg>
    </div>
  )
}

export const PageShow = ({
  currentPainting,
  setOperation,
  onDrawRect,
  onDrawFlood,
  operation,
  onUndo
}) => {
  const [outline, setOutline] = useState('A')
  const [fill, setFill] = useState(' ')

  const [currentRendered, setCurrentRendered] = useState([])
  const [preview, setPreview] = useState([])
  const [rectDrawing, setRectDrawing] = useState({ isDrawing: false })

  const currentDrawingRect = (initial, final) => {
    const [x, y] = initial
    const [rx, ry] = final
    const [width, height] = [rx - x, ry - y]

    const [fixedWidth, fixedHeight] = [
      Math.sign(width || 1) * (Math.abs(width) + 1),
      Math.sign(height || 1) * (Math.abs(height) + 1)
    ]

    return {
      type: 'Rectangule',
      fields: normalize({
        x, y, width: fixedWidth, height: fixedHeight, outline, fill
      })

    }
  }

  const operations = {
    rectangule: {
      onMouseDown: (ix, iy) => {
        const rect = currentDrawingRect([ix, iy], [ix, iy])
        setRectDrawing({
          isDrawing: true,
          rectInitial: [ix, iy],
          rect
        })

        setPreview(render(currentRendered, rect))
      },
      onMouseEnter: (ix, iy) => {
        if (rectDrawing.isDrawing) {
          const rect = currentDrawingRect(rectDrawing.rectInitial, [ix, iy])
          setRectDrawing({
            ...rectDrawing,
            rect
          })

          setPreview(render(currentRendered, rect))
        }
      },
      onMouseUp: (ix, iy) => {
        if (rectDrawing.isDrawing) {
          onDrawRect(rectDrawing.rect.fields)
          setRectDrawing({ isDrawing: false })
        }
      },
      coordProps: (ix, iy) => {
        if (!rectDrawing.isDrawing)
          return { fill: "#00000000" }

        if (inside([ix, iy], rectDrawing.rect.fields))
          return { fill: "#FF000033" }
        else
          return { fill: "#00000000" }
      }
    },
    flood: {
      onMouseDown: (ix, iy) => { },
      onMouseEnter: (ix, iy) => {
        setPreview(render(currentRendered, {
          type: 'Flood',
          fields: {
            fill: fill,
            x: ix,
            y: iy
          }
        }))
      },
      onMouseUp: (ix, iy) => {
        onDrawFlood({
          fill: fill,
          x: ix,
          y: iy
        })
      },
      coordProps: (ix, iy) => {
        if (preview[iy][ix] == currentRendered[iy][ix])
          return { fill: "#00000000" }
        else
          return { fill: "#FF000033" }
      }
    }
  }

  const cellSize = 20

  useEffect(() => {
    const rendered = renderPainting(currentPainting)
    setCurrentRendered(rendered)
    setPreview(rendered)
  }, [currentPainting, operation])



  return (
    <>
      <h1 data-testid="painting-title">Painting {currentPainting.id}</h1>
      <div className="row">
        <div className="column">
          <button onClick={() => setOperation("rectangule")}>Rectangule</button>{" "}
          <button onClick={() => setOperation("flood")}>Flood</button>{" "}
          <button onClick={onUndo}>Undo</button>

          <div className="row">
            <div className="column">
              <fieldset>
                <label>Fill</label>
                <input onChange={(e) => setFill(e.target.value)} value={fill} maxLength="1" />
              </fieldset>
            </div>
            {operation == 'rectangule' && (
              <div className="column">
                <fieldset>
                  <label>
                    Outline
                  </label>
                  <input onChange={(e) => setOutline(e.target.value)} value={outline} maxLength="1" />
                </fieldset>
              </div>
            )}
          </div>
        </div>
      </div>

      <div className="row">
        <div className="column">
          <RenderedCanvas
            preview={preview}
            cellAttrs={
              (ix, iy) => (
                {
                  ...operations[operation].coordProps(ix, iy),
                  onMouseDown: () => operations[operation].onMouseDown(ix, iy),
                  onMouseEnter: () => operations[operation].onMouseEnter(ix, iy),
                  onMouseUp: () => operations[operation].onMouseUp(ix, iy),
                  'data-xy': iy + "-" + ix
                }
              )
            }
          />
        </div>
      </div>
    </>
  )
}
