import { Socket } from 'phoenix'

// alert(`
//   make not equal revision show final result
//   tests
// `)
class OfflineRepo {

  constructor() {
    this.paintings = JSON.parse(localStorage["offline_listing"] || "[]")
    this.pendingActions = JSON.parse(localStorage["pending_actions"] || "[]")

    this.updatePaintings(this.pendingActions)
  }

  updateOfflineListing(paintings) {
    this.paintings = paintings
    localStorage.setItem("offline_listing", JSON.stringify(paintings))
  }

  pushPending(action) {
    this.pendingActions = [...this.pendingActions, action]

    localStorage.setItem("pending_actions", JSON.stringify(this.pendingActions))
  }

  initialize({ onUpdateListing }) {
    onUpdateListing(this.paintings)
  }

  clearLocal(paintingId) {
    this.pendingActions = this.pendingActions.filter(({ paintingId: id }) => id !== paintingId)

    localStorage.setItem("pending_actions", JSON.stringify(this.pendingActions))
  }

  updatePaintings(pendingActions) {
    pendingActions.forEach((action) => {
      if (action.paintingId < 0) {
        if (!this.paintings.find((painting) => painting.id === action.paintingId))
          this.paintings.push({
            id: action.paintingId,
            revision: 1,
            shapes: []
          })
      }

      this.paintings = this.paintings.map((painting) => {
        if (painting.id === action.paintingId) {
          return {
            ...painting,
            shapes: [
              ...painting.shapes,
              action.shape
            ]
          }
        } else {
          return painting
        }
      })
    })
  }

  updatePainting(painting) {
    const paintings = this.paintings.map((p) => p.id === painting.id ? painting : p)
    this.updateOfflineListing(paintings)
  }

  create(params) {
    return new Promise((resolve, reject) => {
      const minId =
        this.paintings
          .map(({ id }) => id)
          .sort((x, y) => x - y)[0]


      const id = Math.min(0, minId || 0) - 1

      const pending = {
        paintingId: id,
        shape: {
          type: "Bounds",
          fields: params
        }
      }

      this.pushPending(pending)

      this.updatePaintings([pending])

      resolve(this.paintings.find((p) => p.id === pending.paintingId))
    })
  }

  managePainting(painting, { onPaintingUpdated }) {
    return new Promise((resolve) => {
      onPaintingUpdated(painting)
      resolve()
    })
  }

  draw(shapeAction, painting, params) {
    return new Promise((resolve, reject) => {
      let type

      switch (shapeAction) {
        case "draw_rect": type = "Rectangule"; break;
        case "draw_flood": type = "Flood"; break;
        case "undo": reject("Undo not supported offline."); return;
      }

      const pending = {
        paintingId: painting.id,
        shape: {
          type: type,
          fields: params
        }
      }

      this.pushPending(pending)
      this.updatePaintings([pending])
      resolve(this.paintings.find((p) => p.id === pending.paintingId))
    })
  }
}

export const offlineRepo = new OfflineRepo()

export class OnlineRepo {

  initialize({
    onError,
    onClose,
    onConnected
  }) {
    return new Promise((resolve) => {
      this.socket = new Socket("/socket")
      this.socket.onOpen(() => {
        this.channelListing = this.socket.channel("paintings")

        this.channelListing.join().receive("ok", () => {

          onConnected()
          resolve()
        })
      })
      this.socket.onError(onError)
      this.socket.onClose(onClose)

      this.socket.connect()
    })
  }

  manageListing({ onUpdateListing }) {
    const onUpdated = (paintings) => {
      onUpdateListing(paintings)
      offlineRepo.updateOfflineListing(paintings)
    }

    this.channelListing.push("list").receive("ok", (paintings) => onUpdated(paintings))
    this.channelListing.on("list", ({ paintings }) => onUpdated(paintings))
  }

  create(params) {
    return new Promise((resolve, reject) => {
      this.channelListing
        .push("create", params)
        .receive("ok", ({ painting }) => resolve(painting))
        .receive("error", (error) => reject(error))
    })
  }

  getPaintingState(id) {
    return new Promise((resolve, reject) => {
      this.channelListing
        .push("state", { painting_id: id })
        .receive("ok", ({ painting }) => resolve(painting))
        .receive("error", reject)
    })
  }

  managePainting(painting, { onPaintingUpdated } = {}) {
    return new Promise((resolve, reject) => {
      if (this.channelPaintingManager)
        this.channelPaintingManager.leave()

      this.channelPaintingManager = this.socket.channel("painting:" + painting.id)

      this.channelPaintingManager.join()
        .receive("ok", () => {
          this.channelPaintingManager
            .push("state")
            .receive("ok", ({ painting }) => {
              onPaintingUpdated && onPaintingUpdated(painting)
              resolve(painting)
            })
            .receive("error", reject)

          onPaintingUpdated && (
            this.channelPaintingManager.on("updated_painting", ({ painting }) => {
              onPaintingUpdated(painting)
            })
          )
        })
        .receive("error", reject)
    })
  }

  draw(shapeAction, currentPainting, params = {}) {
    return new Promise((resolve, reject) => {
      this.channelPaintingManager
        .push(shapeAction, { ...params, revision: currentPainting.revision })
        .receive("ok", ({ painting }) => {
          offlineRepo.updatePainting(painting)
          resolve(painting)
        })
        .receive("error", ({ errors }) => {
          reject(errors)
        })
    })
  }

  disconnect() {
    this.socket.disconnect()
  }
}