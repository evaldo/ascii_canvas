import React, { useEffect, useMemo, useState } from 'react'
import { render, renderPainting } from './renderer'
import { RenderedCanvas } from './PageShow'

const SyncPainting = ({ id, actions, localPainting, onlineRepo, onClearLocal }) => {
  const [remoteState, setRemoteState] = useState()
  const [send, setSend] = useState('not_sent')
  const [showingPreview, setShowingPreview] = useState(false)

  useEffect(() => {
    if (localPainting.id > 0) {
      onlineRepo
        .getPaintingState(id)
        .then((painting) => {
          setRemoteState(painting)
        })
    }
  }, [id])

  useEffect(() => {
    if (send == 'not_sent' && remoteState?.revision == localPainting.revision) {
      onSend()
    }
  }, [send, remoteState])

  const onSend = async () => {
    setSend("sending")
    let painting = remoteState

    for (const action of actions) {
      if (painting?.id > 0)
        await onlineRepo.managePainting(painting,)

      switch (action.shape.type) {
        case "Rectangule":
          painting = await onlineRepo.draw("draw_rect", painting, action.shape.fields)
          break;

        case "Flood":
          painting = await onlineRepo.draw("draw_flood", painting, action.shape.fields)
          break;

        case "Bounds":
          painting = await onlineRepo.create(action.shape.fields)
      }
    }

    setRemoteState(painting)
    setSend("sent")
    onClearLocal(id)
  }

  const finalResultCanvas = useMemo(() => {
    if (remoteState) {
      return actions.reduce((canvas, action) => {
        return render(canvas, action.shape)
      }, renderPainting(remoteState))
    } else {
      return []
    }
  }, [remoteState, actions])

  return (
    <tr data-testid={`sync-${localPainting?.id}`}>
      <td>
        <div>{id}</div>
        <div>Base local revision: {localPainting.revision}</div>
        <div>Remote revision: {remoteState?.revision}</div>
        {(remoteState?.revision == localPainting.revision || send === 'not_sent') && (
          <button disabled={send !== "not_sent"} onClick={onSend} >
            {
              send === 'not_sent' ? "Send" : (
                send === 'sending' ? "Sending" : (
                  "Sent"
                )
              )
            }
          </button>
        )}
        {send === 'sent' ? "Synchronization successfull!" : (
          <>
            <button onClick={() => setShowingPreview(!showingPreview)}>
              {showingPreview ? "Hide preview" : "Show preview"}
            </button>
            {showingPreview &&
              <>
              <div className="row">
                <div className="column" data-testid="preview-local">
                  Local canvas
                  <RenderedCanvas preview={renderPainting(localPainting)} cellAttrs={() => ({ fill: "#00000000" })} ratio={0.3} />
                  </div>
                </div>
                {remoteState && (
                <>
                  <div className="row">
                    <div className="column" data-testid="preview-remote">
                      Remote canvas
                      <RenderedCanvas preview={renderPainting(remoteState)} cellAttrs={() => ({ fill: "#00000000" })} ratio={0.3} />
                    </div>
                  </div>
                  <div className="row">
                    <div className="column" data-testid="preview-final">
                      After sync
                      <RenderedCanvas preview={finalResultCanvas} cellAttrs={() => ({ fill: "#00000000" })} ratio={0.3} />
                    </div>
                  </div>
                </>
                )}
              </>
            }
          </>
        )}
      </td>
      <td>
        <ul>
          {actions.map((action, ix) => {
            return (
              <li key={ix}>
                {action.shape.type}
              </li>
            )
          })}
        </ul>
      </td>
    </tr>
  )
}

export const Sync = ({
  offlineRepo,
  onlineRepo,
  onSyncEnd
}) => {
  console.info("asdf")
  const groupedByPainting =
    useMemo(() =>
      offlineRepo.pendingActions.reduce((acc, action) => {
        acc[action.paintingId] = acc[action.paintingId] || []

        acc[action.paintingId].push(
          action
        )

        return acc
      }, {}),
      [offlineRepo.pendingActions]
    )

  const onClearLocal = (id) => {
    // TODO id is a string here?
    offlineRepo.clearLocal(parseInt(id))
    if (offlineRepo.pendingActions.length === 0)
      onSyncEnd()
  }

  useEffect(() => {
    if (offlineRepo.pendingActions.length === 0)
      onSyncEnd()
  }, [])

  return (
    <table>
      <thead>
        <tr>
          <th>
            Painting
          </th>
          <th>
            Actions
          </th>
        </tr>
      </thead>
      <tbody>
        {
          Object.keys(groupedByPainting).map((id, ix) => {
            return (
              <SyncPainting
                key={ix}
                id={id}
                actions={groupedByPainting[id.toString()]}
                localPainting={offlineRepo.paintings.find((p) => p.id.toString() === id)}
                onlineRepo={onlineRepo}
                onClearLocal={onClearLocal}
              />
            )
          })
        }
      </tbody>
    </table>
  )
}