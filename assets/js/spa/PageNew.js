import React, { useState } from 'react'

export const PageNew = ({ onNewPainting }) => {
  const [width, setWidth] = useState(0)
  const [height, setHeight] = useState(0)

  const onSubmit = (e) => {
    e.preventDefault()
    onNewPainting({ width, height })
  }

  return (
    <>
      <h1>New painting</h1>
      <form onSubmit={onSubmit}>
        <div>
          Width
          <input type="number" value={width} onChange={(e) => setWidth(e.target.value)} min="0" />
        </div>
        <div>
          Height
          <input type="number" value={height} onChange={(e) => setHeight(e.target.value)} min="0" />
        </div>
        <input type="submit" />
      </form>
    </>
  )
}
