describe('Spa', () => {
  it('should create and edit painting', () => {
    cy.visit('/spa')

    cy.contains("Connecting...").should("not.exist")

    cy.contains("No painting created yet :(")

    cy.contains("Status: Online")

    cy.contains("New Painting").click()

    cy.get("input").eq(0).type("{selectAll}5")
    cy.get("input").eq(1).type("{selectAll}5")

    cy.get("input[type=submit]").click()

    cy.contains("No painting created yet :(").should("not.exist")

    cy.get("[data-xy=2-2]").click()

    cy.contains("Flood").click()
    cy.contains("Outline").should("not.exist")

    cy.get("input").type("{selectAll}B")

    cy.get("[data-xy=1-1]").click()

    cy.get("text").should("have.text", "BBBBBBBBBBBBABBBBBBBBBBBB")
  })

  it("should update local painting automatically", () => {
    cy.visit('/spa')

    cy.contains("No painting created yet :(").should("exist")

    cy.request("POST", "/api/v1/paintings", { width: 3, height: 3 })
      .then((res) => {
        const id = res.body.data.id

        cy.contains("No painting created yet :(").should("not.exist")

        cy.get("#paintings li a").should("have.length", 1).click()

        cy.get("text").should("have.text", "         ")

        cy.request("POST", `/api/v1/paintings/${id}/rectangules`, {
          width: 3,
          height: 3,
          x: 1,
          y: 1,
          outline: "A",
          revision: 0
        })

        cy.get("text").should("have.text", "     AAA A A AAA")
      })
  })

  describe("offline", () => {

    it("should send changes automatically if local and remote revision matches", () => {
      cy.visit("/spa")

      cy.contains("Connecting...").should("not.exist")

      cy.contains("New Painting").click()

      cy.get("input").eq(0).type("{selectAll}5")
      cy.get("input").eq(1).type("{selectAll}5")

      cy.get("input[type=submit]").click()

      cy.get("[data-testid=painting-title]").should("exist")

      cy.contains("Go offline").click()

      cy.contains("Status: Offline").should("exist")

      cy.get("[data-xy=2-2]").click()

      cy.get("text").should("have.text", "            A            ")

      cy.contains("Connect").click()

      cy.contains("Select a painting on the list to start editing or click in the ")

      cy.visit("/spa")
      cy.get("[data-testid^=show-painting-]").click()

      cy.get("text").should("have.text", "            A            ")
    })

    it("should show changes if local and remote revisions don't match", () => {
      cy.request("POST", "/api/v1/paintings", { width: 3, height: 3 })
        .then((res) => {
          const id = res.body.data.id

          cy.visit("/spa")

          cy.get("[data-testid^=show-painting-").click()

          cy.contains("Go offline").click().contains("Connect")

          cy.get("[data-xy=0-0]").click()

          cy.get("text").should("have.text", "A        ")

          cy.request("POST", `/api/v1/paintings/${id}/rectangules`, {
            width: 3,
            height: 3,
            x: 1,
            y: 1,
            outline: "A",
            revision: 0
          })

          cy.get("text").should("have.text", "A        ")

          cy.contains("Connect").click()

          cy.get(`[data-testid=sync-${id}]`).should("exist")

          cy.contains("Show preview").click()

          cy.get("[data-testid='preview-local'] text").should("have.text", "A        ")
          cy.get("[data-testid='preview-remote'] text").should("have.text", "     AAA A A AAA")
          cy.get("[data-testid='preview-final'] text").should("have.text", "A    AAA A A AAA")

          cy.contains("Send").click()

          cy.get("[data-testid^=show-painting-").click()

          cy.get("text").should("have.text", "A    AAA A A AAA")
        })
    })

    it("should sync newly created painting", () => {
      cy.visit("/spa")

      cy.contains("Status: Online").should("exist")
      cy.contains("Go offline").click().contains("Connect")

      cy.contains("New Painting").click()

      cy.get("input").eq(0).type("{selectAll}5")
      cy.get("input").eq(1).type("{selectAll}5")

      cy.get("input[type=submit]").click()
      cy.get("[data-xy=0-0]").click()

      cy.contains("Connect").click()
      cy.contains("Send").click()

      cy.get("[data-testid^=show-painting-").click()

      cy.get("text").should("have.text", "A                        ")
    })
  })

})