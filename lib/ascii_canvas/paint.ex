defmodule AsciiCanvas.Paint do
  @moduledoc """
  The context module to interact with persisted paintings.
  """
  alias AsciiCanvas.Repo
  alias AsciiCanvas.Paint.{
    PaintingAction,
    ShapesType,
    Painting
  }
  alias AsciiCanvas.Paint.Shape.{
    Rectangule,
    Flood,
    Bounds
  }


  import Ecto.Query

  @doc """
  Creates a new painting.

  When successfull broadcasts `{:created_paiting, painting}` message into `painting` pubsub topic.
  """
  def create_painting(params \\ %{}) do
    %Bounds{}
    |> Bounds.changeset(params)
    |> Ecto.Changeset.apply_action(:insert)
    |> case do
      {:ok, bounds} ->
        {:ok, painting} =
          %Painting{}
          |> Painting.changeset(%{})
          |> Ecto.Changeset.put_change(:shapes, [bounds])
          |> Ecto.Changeset.put_assoc(:actions, [%PaintingAction{action: "append", payload: [bounds]}])
          |> Repo.insert()

        Phoenix.PubSub.broadcast(AsciiCanvas.PubSub, "painting", {:created_painting, painting})

        {:ok, painting}

      error ->
        error
    end
  end

  defp update_painting(%{data: %Painting{} = painting} = changeset) do
    changeset
    |> Painting.validate_changes()
    |> Repo.update()
    |> case do
      {:ok, painting} ->
        Phoenix.PubSub.broadcast(
          AsciiCanvas.PubSub,
          "painting:#{painting.id}",
          {:updated_painting, painting}
        )

        {:ok, painting}

      error ->
        error
    end
  end

  @doc """
  Applies the given rect to the painting.

  Returns a Painting changeset with the operation applied.
  """
  def draw_rectangule(%Painting{shapes: shapes} = painting, rect_params) do
    %Rectangule{}
    |> Rectangule.changeset(rect_params)
    |> Ecto.Changeset.apply_action(:insert)
    |> case do
      {:ok, rect} ->
        painting
        |> Painting.changeset(rect_params)
        |> Ecto.Changeset.put_change(:shapes, shapes ++ [rect])
        |> Ecto.Changeset.put_assoc(:actions, painting.actions ++ [%PaintingAction{action: "append", payload: [rect]}])
        |> update_painting()

      error ->
        error
    end
  end

  @doc """
  Floods a point in the picture with the given `fill`.

  Returns a Painting changeset with the operation applied.
  """
  def flood(%Painting{shapes: shapes} = painting, flood_params) do
    %Flood{}
    |> Flood.changeset(flood_params)
    |> Ecto.Changeset.apply_action(:insert)
    |> case do
      {:ok, flood} ->
        painting
        |> Painting.changeset(flood_params)
        |> Ecto.Changeset.put_change(:shapes, shapes ++ [flood])
        |> Ecto.Changeset.put_assoc(:actions, painting.actions ++ [%PaintingAction{action: "append", payload: [flood]}])
        |> update_painting()

      error ->
        error
    end
  end

  @doc """
  Gets a `Painting` from the database.

  Raises if none is found.
  """
  def get_painting!(id) do
    Painting
    |> preload(:actions)
    |> Repo.get!(id)
  end

  def get_painting(id) do
    Painting
    |> preload(:actions)
    |> Repo.get(id)
  end

  @doc """
  List paintings ordered by insertion decrescent.
  """
  def list_last_paintings(count) do
    from(
      Painting,
      order_by: [desc: :inserted_at],
      limit: ^count
    )
    |> Repo.all()
  end

  def serialize(painting) do
    {:ok, shapes} = ShapesType.dump(painting.shapes)

    %{
      id: painting.id,
      revision: painting.revision,
      shapes: shapes
    }
  end

  def undo(%Painting{shapes: shapes, actions: actions} = painting, params) do
    case shapes do
      [] ->
        {:error, :invalid_state}

      [_bounds] ->
        {:ok, painting}

      _ ->
        last = List.last(shapes)

        painting
        |> Painting.changeset(params)
        |> Ecto.Changeset.put_change(:shapes, List.delete(shapes, last))
        |> Ecto.Changeset.put_assoc(:actions, actions ++ [%PaintingAction{action: "undo", payload: [last]}])
        |> update_painting()

    end
  end
end
