defmodule AsciiCanvas.Paint.PaintingAction do
  use Ecto.Schema
  import Ecto.Changeset

  alias AsciiCanvas.Paint.Painting
  alias AsciiCanvas.Paint.ShapesType

  schema "painting_actions" do
    field :action
    field :payload, ShapesType

    belongs_to :painting, Painting

    timestamps()
  end

  def changeset(painting_action, attrs) do
    painting_action
    |> cast(attrs, [:action, :payload])
    |> validate_required(:action)
  end

end
