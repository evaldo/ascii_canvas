defmodule AsciiCanvas.Paint.Canvas do
  @moduledoc """
  The Canvas operations module.
  """

  @type canvas :: list(list())
  @type x :: integer()
  @type y :: integer()
  @type coordinate :: {integer(), integer()}
  @type width :: integer()
  @type height :: integer()
  @type rect :: {x, y, width, height}
  @type draw_result :: canvas() | {:error, String.t()}

  alias AsciiCanvas.Paint.Shape

  def render(shapes) do
    Enum.reduce(shapes, [], &Shape.render/2)
  end

  @spec to_string(canvas(), String.t()) :: String.t()
  def to_string(canvas, nil_char \\ " ") do
    canvas
    |> Enum.map(fn line ->
      line
      |> Enum.map(fn
        nil -> nil_char
        char -> char
      end)
    end)
    |> Enum.join("\n")
  end
end
