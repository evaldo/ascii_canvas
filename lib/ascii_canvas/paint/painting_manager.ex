defmodule AsciiCanvas.Paint.PaintingManager do
  use GenServer, restart: :temporary

  alias AsciiCanvas.Paint

  def init({painting_id, from}) do
    painting = Paint.get_painting!(painting_id)
    Process.link(from)
    Process.flag(:trap_exit, true)

    {:ok, %{painting: painting, clients: [from]}}
  end

  def start_link(painting_id) do
    GenServer.start_link(__MODULE__, painting_id)
  end

  def register_online(pid_server, client_pid) do
    GenServer.cast(pid_server, {:register_online, client_pid})
  end

  def flood(pid_server, params) do
    GenServer.call(pid_server, {:flood, params})
  end

  def undo(pid_server, params) do
    GenServer.call(pid_server, {:undo, params})
  end

  def rectangule(pid_server, params) do
    GenServer.call(pid_server, {:rectangule, params})
  end

  def handle_call({:flood, params}, _from, state) do
    case Paint.flood(state.painting, params) do
      {:ok, painting} ->
        {:reply, {:ok, painting}, %{state | painting: painting}}

      error ->
        {:reply, error, state}
    end
  end

  def handle_call({:rectangule, params}, _from, state) do
    case Paint.draw_rectangule(state.painting, params) do
      {:ok, painting} ->
        {:reply, {:ok, painting}, %{state | painting: painting}}

      error ->
        {:reply, error, state}
    end
  end

  def handle_call({:undo, params}, _from, state) do
    case Paint.undo(state.painting, params) do
      {:ok, painting} ->
        {:reply, {:ok, painting}, %{state | painting: painting}}

      error ->
        {:reply, error, state}
    end
  end

  def handle_cast({:register_online, pid}, state) do
    Process.link(pid)

    {
      :noreply,
      Map.put(state, :clients, [pid | state.clients])
    }
  end

  def handle_info({:EXIT, pid, _reason}, state) do
    clients = List.delete(state.clients, pid)
    state = Map.put(state, :clients, clients)

    if Enum.empty?(clients) do
      {:stop, :normal, state}
    else
      {:noreply, state}
    end
  end
end
