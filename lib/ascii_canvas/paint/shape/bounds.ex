defmodule AsciiCanvas.Paint.Shape.Bounds do
  use Ecto.Schema

  import Ecto.Changeset

  embedded_schema do
    field :width, :integer, default: 0
    field :height, :integer, default: 0
  end

  def changeset(bounds, params) do
    bounds
    |> cast(params, [:width, :height])
    |> validate_non_negative(:width)
    |> validate_non_negative(:height)
  end

  defp validate_non_negative(changeset, field) do
    changeset
    |> validate_change(field, fn _field, value ->
      case value do
        nil ->
          []

        negative when negative < 0 ->
          [{field, "must not be negative"}]

        _ ->
          []
      end
    end)
  end

end

defimpl AsciiCanvas.Paint.Shape, for: AsciiCanvas.Paint.Shape.Bounds do

  alias AsciiCanvas.Paint.Shape.Bounds

  def render(%Bounds{width: width, height: height}, []) do
    if width == 0 or height == 0 do
      []
    else
      for _y <- 1..height do
        for _x <- 1..width do
          nil
        end
      end
    end

  end

  def render(_, _), do: raise "Canvas must be empty to apply initial bounds"

end
