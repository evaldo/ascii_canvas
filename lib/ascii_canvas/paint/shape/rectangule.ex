defmodule AsciiCanvas.Paint.Shape.Rectangule do
  use Ecto.Schema

  import Ecto.Changeset

  embedded_schema do
    field :x, :integer
    field :y, :integer
    field :width, :integer
    field :height, :integer
    field :fill, :string
    field :outline, :string
  end

  def changeset(rectangule, params) do
    rectangule
    |> cast(params, [:x, :y, :width, :height, :fill, :outline])
    |> validate_required([:x, :y, :width, :height])
    |> validate_length(:fill, max: 1)
    |> validate_length(:outline, max: 1)
    |> validate_fill_or_outline
  end

  defp validate_fill_or_outline(changeset) do
    if get_field(changeset, :fill) || get_field(changeset, :outline) do
      changeset
    else
      changeset
      |> add_error(:fill, "fill or outline must be provided")
      |> add_error(:outline, "fill or outline must be provided")
    end
  end
end

defimpl AsciiCanvas.Paint.Shape, for: AsciiCanvas.Paint.Shape.Rectangule do

  alias AsciiCanvas.Paint.Shape.Rectangule
  alias AsciiCanvas.Paint.CanvasHelper

  def render(%Rectangule{outline: outline, fill: fill} = rect, canvas) do
    {rect_x, rect_y, width, height} = rect = normalize(rect)

    canvas_initial_x = min(0, rect_x)
    canvas_initial_y = min(0, rect_y)

    {current_width, current_height} = CanvasHelper.dimensions(canvas)

    canvas_final_x = max(current_width, rect_x + width)
    canvas_final_y = max(current_height, rect_y + height)

    for y <- canvas_initial_y..(canvas_final_y - 1) do
      for x <- canvas_initial_x..(canvas_final_x - 1) do
        if inside?({x, y}, rect) do
          if outline?({x, y}, rect) do
            outline || fill
          else
            fill
          end
        else
          if x >= 0 and y >= 0 do
            CanvasHelper.pixel_at(canvas, {x, y})
          else
            nil
          end
        end
      end
    end
  end

  defp normalize(%Rectangule{x: x, y: y, width: width, height: height}) do
    {x, width} =
      if width < 0 do
        {x - abs(width) + 1, abs(width)}
      else
        {x, width}
      end

    {y, height} =
      if height < 0 do
        {y - abs(height) + 1, abs(height)}
      else
        {y, height}
      end

    {x, y, width, height}
  end

  defp inside?({x, y}, {rect_x, rect_y, _, _} = rect) do
    {right_x, right_y} = rect_right_bottom(rect)

    x >= rect_x and
      x <= right_x and
      y >= rect_y and
      y <= right_y
  end

  defp outline?({x, y}, {rect_x, rect_y, _, _} = rect) do
    {right_x, right_y} = rect_right_bottom(rect)

    x in [rect_x, right_x] or y in [rect_y, right_y]
  end

  defp rect_right_bottom({rect_x, rect_y, width, height}) do
    right_x = rect_x + width - 1
    right_y = rect_y + height - 1

    {right_x, right_y}
  end
end
