defmodule AsciiCanvas.Paint.Shape.Flood do
  @moduledoc """
  Flood operation changeset to be used to validate params from clients
  and parsing to be used and applied on paintings.
  """
  use Ecto.Schema

  import Ecto.Changeset

  embedded_schema do
    field :x, :integer
    field :y, :integer
    field :fill, :string
  end

  def changeset(flood, params) do
    flood
    |> cast(params, [:x, :y, :fill])
    |> validate_required([:x, :y, :fill], trim: false)
    |> validate_length(:fill, max: 1)
  end
end

defimpl AsciiCanvas.Paint.Shape, for: AsciiCanvas.Paint.Shape.Flood do

  alias AsciiCanvas.Paint.Shape.Flood
  alias AsciiCanvas.Paint.CanvasHelper

  def render(%Flood{x: x, y: y, fill: fill}, canvas) do
    initial = CanvasHelper.pixel_at(canvas, {x, y})

    {canvas, _} = flood({canvas, []}, {x, y}, fill, initial)

    canvas
  end

  defp flood({canvas, visited}, {x, y}, fill, initial) do
    should_change? =
      canvas
      |> CanvasHelper.pixel_at({x, y})
      |> Kernel.==(initial)
      |> Kernel.and({x, y} not in visited)

    if should_change? and CanvasHelper.inbounds?(canvas, {x, y}) do
      {
        List.update_at(
          canvas,
          y,
          &List.replace_at(&1, x, fill)
        ),
        [{x, y} | visited]
      }
      |> flood({x - 1, y}, fill, initial)
      |> flood({x + 1, y}, fill, initial)
      |> flood({x, y - 1}, fill, initial)
      |> flood({x, y + 1}, fill, initial)
    else
      {canvas, [{x, y} | visited]}
    end
  end
end
