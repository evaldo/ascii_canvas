defmodule AsciiCanvas.Paint.ShapesType do
  use Ecto.Type

  def type, do: {:array, :map}

  def cast(v), do: {:ok, v}

  def dump(list) do
    {
      :ok,
      list
      |> Enum.map(fn %module{} = struct ->
        %{
          type:
            module
            |> Atom.to_string()
            |> String.replace("Elixir.AsciiCanvas.Paint.Shape.", ""),
          fields: Map.from_struct(struct)
        }
      end)
    }
  end

  def load(maps) do
    {
      :ok,
      maps
      |> Enum.map(fn %{"type" => module, "fields" => fields} ->
        module = String.to_existing_atom("Elixir.AsciiCanvas.Paint.Shape." <> module)
        fields = Enum.map(fields, fn {k, v} -> {String.to_existing_atom(k), v} end)

        struct!(module, fields)
      end)
    }
  rescue
    _ -> :error
  end
end
