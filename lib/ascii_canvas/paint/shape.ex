defprotocol AsciiCanvas.Paint.Shape do

  @spec render(t(), list(list())) :: list(list())
  def render(shape, canvas)

end

defmodule AsciiCanvas.Paint.CanvasHelper do

  def dimensions(canvas) do
    current_width =
      canvas
      |> Enum.map(&length/1)
      |> Enum.max(fn -> 0 end)

    current_height = length(canvas)

    {current_width, current_height}
  end

  def inbounds?(canvas, {x, y}) do
    if x >= 0 and y >= 0 do
      {max_x, max_y} = dimensions(canvas)

      x < max_x and y < max_y
    else
      false
    end
  end

  def pixel_at(canvas, {x, y}) do
    canvas
    |> Enum.at(y)
    |> Kernel.||([])
    |> Enum.at(x)
  end
end
