defmodule AsciiCanvas.Paint.Painting do
  use Ecto.Schema

  alias AsciiCanvas.Paint.ShapesType
  alias AsciiCanvas.Paint.PaintingAction

  import Ecto.Changeset

  schema "paintings" do
    field :shapes, ShapesType, default: []
    field :revision, :integer, default: 0

    has_many :actions, PaintingAction

    timestamps()
  end

  def changeset(canvas, attrs) do
    canvas
    |> cast(attrs, [:shapes, :revision])
    |> cast_assoc(:actions)
  end

  def validate_changes(changeset) do
    given_revision = changeset.params[:revision] || changeset.params["revision"]

    if given_revision == changeset.data.revision do
      put_change(changeset, :revision, (get_field(changeset, :revision) || 0) + 1)
    else
      add_error(changeset, :revision, "not given or different from the one in the server")
    end
  end

end
