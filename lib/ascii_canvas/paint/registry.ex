defmodule AsciiCanvas.Paint.Registry do
  use GenServer

  alias AsciiCanvas.Paint.PaintingManager

  def init(_arg) do
    Process.flag(:trap_exit, true)
    {:ok, Map.new()}
  end

  def start_link(arg) do
    GenServer.start_link(__MODULE__, arg, name: :paint_registry)
  end

  def painting_pid(painting_id) do
    GenServer.call(:paint_registry, {:painting_pid, painting_id})
  end

  def handle_call({:painting_pid, painting_id}, {from, _}, state) do
    {:ok, painting_id} = Ecto.Type.cast(:integer, painting_id)

    case Map.fetch(state, painting_id) do
      {:ok, pid} ->
        PaintingManager.register_online(pid, from)
        {:reply, pid, state}

      _ ->
        {:ok, pid} =
          DynamicSupervisor.start_child(
            AsciiCanvas.Paint.PaintingSupervisor,
            {PaintingManager, {painting_id, from}}
          )

        Process.link(pid)
        {:reply, pid, Map.put(state, painting_id, pid)}
    end
  end

  def handle_info({:EXIT, pid, _reason}, state) do
    key =
      Enum.find_value(state, fn
        {key, ^pid} -> key
        _ -> nil
      end)

    {:noreply, Map.delete(state, key)}
  end
end
