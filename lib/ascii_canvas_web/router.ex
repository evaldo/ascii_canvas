defmodule AsciiCanvasWeb.Router do
  use AsciiCanvasWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {AsciiCanvasWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AsciiCanvasWeb do
    pipe_through :browser

    get "/spa", SpaController, :index

    live "/", PaintingLive.Index, :index
    live "/new", PaintingLive.Index, :new
    live "/:id", PaintingLive.Index, :show
  end

  scope "/api/v1", AsciiCanvasWeb.Api.V1 do
    pipe_through :api

    resources "/paintings", PaintingController, only: [:create, :show]
    post "/paintings/:id/rectangules", PaintingController, :draw_rectangules
    post "/paintings/:id/floods", PaintingController, :flood
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: AsciiCanvasWeb.Telemetry
    end

    scope "/" do
      pipe_through :api
      post "/integration/reset", AsciiCanvasWeb.IntegrationController, :reset
    end
  end
end
