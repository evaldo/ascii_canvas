defmodule AsciiCanvasWeb.PaintingsChannel do
  use AsciiCanvasWeb, :channel

  alias AsciiCanvas.Paint
  alias AsciiCanvas.ChangesetHelper

  def join("paintings", _params, socket) do
    :ok = AsciiCanvasWeb.Endpoint.subscribe("painting")

    {:ok, socket}
  end

  def handle_in("list", _params, socket) do
    paintings =
      Paint.list_last_paintings(5)
      |> Enum.map(&Paint.serialize/1)

    {:reply, {:ok, paintings}, socket}
  end

  def handle_in("create", params, socket) do
    case Paint.create_painting(params) do
      {:ok, painting} ->
        serialized = Paint.serialize(painting)

        {:reply, {:ok, %{painting: serialized}}, socket}

      {:error, changeset} ->
        errors = ChangesetHelper.traverse_errors(changeset)

        {:reply, {:error, %{error: errors}}, socket}
    end
  end

  def handle_in("state", %{"painting_id" => id}, socket) do
    case Paint.get_painting(id) do
      nil ->
        {:reply, {:error, "Painting does not exist"}, socket}

      painting ->
        {:reply, {:ok, %{painting: Paint.serialize(painting)}}, socket}
    end
  end

  def handle_info({:created_painting, painting}, socket) do
    paintings =
      Paint.list_last_paintings(5)
      |> Enum.map(&Paint.serialize/1)

    push(socket, "created_painting", %{painting: Paint.serialize(painting)})
    push(socket, "list", %{paintings: paintings})
    {:noreply, socket}
  end
end
