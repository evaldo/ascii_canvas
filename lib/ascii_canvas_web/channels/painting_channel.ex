defmodule AsciiCanvasWeb.PaintingChannel do
  use AsciiCanvasWeb, :channel

  alias AsciiCanvas.Paint
  alias AsciiCanvas.Paint.Registry
  alias AsciiCanvas.Paint.PaintingManager
  alias AsciiCanvas.ChangesetHelper

  def join("painting:" <> id, _params, socket) do
    painting = Paint.get_painting!(id)

    pid = Registry.painting_pid(id)

    {
      :ok,
      socket
      |> assign(pid: pid)
      |> assign(painting: painting)
    }
  end

  def handle_info({:updated_painting, painting}, socket) do
    push(socket, "updated_painting", %{painting: Paint.serialize((painting))})
    {:noreply, assign(socket, painting: painting)}
  end


  def handle_in("state", _params, socket) do
    serialized = socket.assigns.painting |> Paint.serialize()

    {:reply, {:ok, %{painting: serialized}}, socket}
  end

  def handle_in("draw_rect", params, socket) do
    paint_manager_action(
      fn -> PaintingManager.rectangule(socket.assigns.pid, params) end,
      socket
    )
  end

  def handle_in("draw_flood", params, socket) do
    paint_manager_action(
      fn -> PaintingManager.flood(socket.assigns.pid, params) end,
      socket
    )
  end

  def handle_in("undo", params, socket) do
    paint_manager_action(
      fn -> PaintingManager.undo(socket.assigns.pid, params) end,
      socket
    )
  end

  defp paint_manager_action(action, socket) do
    case action.() do
      {:ok, painting} ->
        {
          :reply,
          {:ok, %{painting: Paint.serialize(painting)}},
          assign(socket, painting: painting)
        }

      {:error, changeset} ->
        errors = ChangesetHelper.traverse_errors(changeset)

        response =
          {
            :error, %{
              painting: Paint.serialize(socket.assigns.painting),
              errors: errors
            }
          }

        {:reply, response, socket}
    end
  end

end
