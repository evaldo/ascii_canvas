defmodule AsciiCanvasWeb.Api.V1.PaintingView do
  use AsciiCanvasWeb, :view

  alias AsciiCanvas.Paint.Canvas

  def render("show.json", %{painting: painting}) do
    %{
      data: %{
        id: painting.id,
        canvas: Canvas.render(painting.shapes)
      }
    }
  end
end
