defmodule AsciiCanvasWeb.Api.FallbackView do
  use AsciiCanvasWeb, :view

  def render("error.json", %{changeset: changeset}) do
    Ecto.Changeset.traverse_errors(changeset, fn {message, options} ->
      Enum.reduce(options, message, fn {type, payload}, message ->
        String.replace(message, "%{#{type}}", to_string(payload))
      end)
    end)
  end
end
