defmodule AsciiCanvasWeb.IntegrationController do
  use AsciiCanvasWeb, :controller

  alias AsciiCanvas.Paint.{
    Painting,
    PaintingAction
  }
  alias AsciiCanvas.Repo

  def reset(conn, _params) do
    Repo.delete_all(PaintingAction)
    Repo.delete_all(Painting)

    send_resp(conn, 200, "Data deleted")
  end

end
