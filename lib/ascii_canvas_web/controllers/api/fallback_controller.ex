defmodule AsciiCanvasWeb.Api.FallbackController do
  use AsciiCanvasWeb, :controller

  alias Ecto.Changeset

  def call(conn, {:error, %Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(AsciiCanvasWeb.Api.FallbackView)
    |> render("error.json", changeset: changeset)
  end
end
