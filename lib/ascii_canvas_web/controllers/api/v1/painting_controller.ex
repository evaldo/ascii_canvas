defmodule AsciiCanvasWeb.Api.V1.PaintingController do
  use AsciiCanvasWeb, :controller

  alias AsciiCanvas.Paint
  alias AsciiCanvas.Paint.Registry
  alias AsciiCanvas.Paint.PaintingManager

  action_fallback AsciiCanvasWeb.Api.FallbackController

  def create(conn, params) do
    {:ok, painting} = Paint.create_painting(params)

    conn
    |> put_status(:created)
    |> put_resp_header("location", Routes.painting_path(conn, :show, painting))
    |> render("show.json", painting: painting)
  end

  def show(conn, %{"id" => id}) do
    painting = Paint.get_painting!(id)

    render(conn, "show.json", painting: painting)
  end

  def draw_rectangules(conn, %{"id" => id} = params) do
    pid = Registry.painting_pid(id)

    case PaintingManager.rectangule(pid, params) do
      {:ok, painting} ->
        conn
        |> put_status(:ok)
        |> render("show.json", painting: painting)

      error ->
        error
    end
  end

  def flood(conn, %{"id" => id} = params) do
    pid = Registry.painting_pid(id)

    case PaintingManager.flood(pid, params) do
      {:ok, painting} ->
        conn
        |> put_status(:ok)
        |> render("show.json", painting: painting)

      error ->
        error
    end
  end
end
