defmodule AsciiCanvasWeb.PaintingLive.RectanguleFormComponent do
  use AsciiCanvasWeb, :live_component

  alias AsciiCanvas.Paint.Shape.Rectangule
  alias AsciiCanvas.Paint.PaintingManager

  @impl true
  def update(assigns, socket) do
    {
      :ok,
      socket
      |> assign(painting_manager_pid: assigns.painting_manager_pid)
      |> assign(changeset: maybe_do_operation(assigns, socket))
      |> assign(draw_operation: assigns.draw_operation)
      |> assign(revision: assigns.revision)
    }
  end

  @impl true
  def handle_event("update_rectangule", %{"rectangule" => params}, socket) do
    changeset =
      %Rectangule{}
      |> Rectangule.changeset(params)
      |> Map.put(:action, :insert)

    {:noreply, assign(socket, changeset: changeset)}
  end

  defp maybe_do_operation(
         %{draw_operation: "rectangule", components_draw: {coordinates, _painting}},
         socket
       ) do
    params =
     socket.assigns.changeset.params
     |> Map.merge(coordinates)
     |> Map.put("revision", socket.assigns.revision)

    case PaintingManager.rectangule(socket.assigns.painting_manager_pid, params) do
      {:ok, _} ->
        send(self(), :draw_handled)

        socket.assigns.changeset

      {:error, changeset} ->
        changeset
    end
  end

  defp maybe_do_operation(_, %{assigns: %{changeset: changeset}}), do: changeset
  defp maybe_do_operation(_, _), do: Rectangule.changeset(%Rectangule{}, %{})
end
