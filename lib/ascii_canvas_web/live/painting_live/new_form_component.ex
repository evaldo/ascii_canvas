defmodule AsciiCanvasWeb.PaintingLive.NewFormComponent do
  use AsciiCanvasWeb, :live_component

  alias AsciiCanvas.Paint

  @impl true
  def update(_assigns, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_event("create", _, socket) do
    {:ok, painting} = Paint.create_painting()

    {
      :noreply,
      socket
      |> push_patch(to: Routes.painting_index_path(socket, :show, painting))
    }
  end
end
