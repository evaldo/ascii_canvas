defmodule AsciiCanvasWeb.PaintingLive.Index do
  use AsciiCanvasWeb, :live_view

  alias AsciiCanvas.Paint
  alias AsciiCanvas.Paint.Canvas
  alias AsciiCanvas.Paint.Registry

  alias AsciiCanvasWeb.PaintingLive.{
    NewFormComponent,
    RectanguleFormComponent,
    FloodFormComponent
  }

  @impl true
  def mount(_params, _session, socket) do
    if connected?(socket) do
      Process.flag(:trap_exit, true)
      Phoenix.PubSub.subscribe(AsciiCanvas.PubSub, "painting")
    end

    {
      :ok,
      socket
      |> assign(draw_operation: "rectangule")
      |> assign(components_draw: nil)
      |> assign(online: false)
      |> assign_paintings()
    }
  end

  @impl true
  def handle_params(params, _url, socket) do
    socket =
      case socket.assigns.live_action do
        :show ->
          %{"id" => id} = params

          painting = Paint.get_painting!(id)

          if current_painting = Map.get(socket.assigns, :painting) do
            Phoenix.PubSub.unsubscribe(AsciiCanvas.PubSub, "painting:#{current_painting.id}")
          end

          Phoenix.PubSub.subscribe(AsciiCanvas.PubSub, "painting:#{id}")

          socket
          |> connect_registry(id)
          |> assign_painting(painting)

        _ ->
          socket
      end

    {:noreply, socket}
  end

  defp connect_registry(socket, painting_id) do
    pid = Registry.painting_pid(painting_id)

    socket
    |> assign(painting_manager_pid: pid)
    |> assign(online: true)
  end

  defp assign_painting(socket, painting) do
    socket
    |> assign(painting: painting)
    |> assign(rendered: Canvas.render(painting.shapes))
  end

  @impl true
  def handle_event("draw", coordinates, socket) do
    {:noreply, assign(socket, components_draw: {coordinates, socket.assigns.painting})}
  end

  @impl true
  def handle_event("draw_operation", %{"operation" => operation}, socket) do
    {:noreply, assign(socket, draw_operation: operation)}
  end

  @impl true
  def handle_info({:updated_painting, painting}, socket) do
    {:noreply, assign_painting(socket, painting)}
  end

  @impl true
  def handle_info({:created_painting, _painting}, socket) do
    {:noreply, assign_paintings(socket)}
  end

  @impl true
  def handle_info(:draw_handled, socket) do
    {:noreply, assign(socket, components_draw: nil)}
  end

  def handle_info(:reconnect, socket) do
    {
      :noreply,
      connect_registry(socket, socket.assigns.painting.id)
    }
  end

  def handle_info({:EXIT, _pid, _reason}, socket) do
    Process.send_after(self(), :reconnect, 4000)

    {
      :noreply,
      socket
      |> assign(painting_pid: nil)
      |> assign(online: false)
    }
  end

  defp assign_paintings(socket) do
    paintings = Paint.list_last_paintings(10)

    assign(socket, paintings: paintings)
  end
end
